data "docker_registry_image" "image" {
  name = var.image
}

resource "docker_image" "image" {
  name          = data.docker_registry_image.image.name
  pull_triggers = [data.docker_registry_image.image.sha256_digest]
  keep_locally = true
}

resource "docker_container" "container" {
  image = docker_image.image.latest
  name  = var.name
  destroy_grace_seconds = var.destroy_grace_seconds
  restart = var.restart

  labels {
    label = "image"
    value = docker_image.image.name
  }

  dynamic "labels" {
    for_each = var.traefik_router == null ? toset([]) : toset([1])

    content {
      label = "traefik.enable"
      value = "true"
    }
  }

  dynamic "labels" {
    for_each = var.traefik_router == null ? toset([]) : toset([1])

    content {
      label = "traefik.http.routers.${var.name}-https.rule"
      value = "Host(`${var.traefik_router.domain}`)"
    }
  }

  dynamic "labels" {
    for_each = var.traefik_router == null ? toset([]) : toset([1])

    content {
      label = "traefik.http.routers.${var.name}-https.entrypoints"
      value = "https"
    }
  }

  dynamic "labels" {
    for_each = var.traefik_router == null ? toset([]) : toset([1])

    content {
      label = "traefik.http.services.${var.name}.loadbalancer.server.port"
      value = "${var.traefik_router.port}"
    }
  }

  dynamic "labels" {
    for_each = try(var.traefik_router.middlewares, null) == null ? toset([]) : toset([1])

    content {
      label = "traefik.http.routers.${var.name}.middlewares"
      value = join(",", var.traefik_router.middlewares)
    }
  }

  dynamic "labels" {
    for_each = var.traefik_router == null ? toset([]) : toset([1])

    content {
      label = "traefik.docker.network"
      value = "${var.traefik_router.network}"
    }
  }

  dynamic "labels" {
    for_each = var.labels == null ? tomap({}) : var.labels
    iterator = each

    content {
      label = each.key
      value = each.value
    }
  }

  dynamic "labels" {
    for_each = var.stack == null ? [] : [var.stack]
    iterator = each

    content {
      label = "com.docker.compose.project"
      value = each.value
    }
  }

  dynamic "networks_advanced" {
    for_each = var.networks
    iterator = each

    content {
      name = each.value.id
    }
  }

  dynamic "volumes" {
    for_each = var.volumes == null ? tomap({}) : var.volumes
    iterator = each

    content {
      container_path = each.value.container_path
      host_path = each.value.host_path
      read_only = each.value.read_only
    }
  }

  dynamic "healthcheck" {
    for_each = var.healthcheck == null ? toset([]) : toset([var.healthcheck])

    content {
      test = var.healthcheck.test
      interval = var.healthcheck.interval == null ? "30s" : var.healthcheck.interval
      retries = var.healthcheck.retries
      start_period = var.healthcheck.start_period
      timeout = var.healthcheck.timeout == null ? "10s" : var.healthcheck.timeout
    }
  }

  dynamic "upload" {
    for_each = var.upload == null ? tomap({}) : var.upload
    iterator = each

    content {
      file = each.value.file
      content = each.value.content
      content_base64 = each.value.content_base64
      executable = each.value.executable
      source = each.value.source
    }
  }

  dynamic "ports" {
    for_each = var.ports
    iterator = each

    content {
      internal = each.value.internal
      external = each.value.external
      protocol = each.value.protocol
    }
  }

  dynamic "devices" {
    for_each = var.devices == null ? [] : var.devices
    iterator = each

    content {
      host_path = each.value.host_path
      container_path = each.value.container_path
      permissions = each.value.permissions
    }
  }

  dynamic "capabilities" {
    for_each = var.capabilities == null ? toset([]) : toset([1])
    iterator = each

    content {
      add = var.capabilities.add
      drop = var.capabilities.drop
    }
  }


  network_mode = var.network_mode
  attach = var.attach
  must_run = var.must_run
  command = var.command

  env = var.environment
  user = var.user

  stop_signal = var.stop_signal
  privileged = var.privileged
}
