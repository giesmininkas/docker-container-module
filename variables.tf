variable "image" {
  type = string
}

variable "name" {
  type = string
}

variable "networks" {
  type = map(object({
    id = string
  }))
  default = {}
}

variable "restart" {
  type = string
}

variable "destroy_grace_seconds" {
  type = number
  default = 30
}

variable "traefik_router" {
  type = object({
    domain = string
    port = number
    network = string
    middlewares = optional(list(string))
  })
  default = null
}

variable "labels" {
  type = map(string)
  default = null
}

variable "environment" {
  type = set(string)
  default = []
}

variable "volumes" {
  type = map(object({
    container_path = string
    host_path = string
    read_only = bool
  }))
  default = null
}

variable "user" {
  type = string
  default = null
}

variable "healthcheck" {
  type = object({
    test = list(string)
    interval = optional(string)
    retries = optional(number)
    start_period = optional(string)
    timeout = optional(string)
  })
  default = null
}

variable "network_mode" {
  type = string
  default = null
}

variable "attach" {
  type = bool
  default = null
}

variable "must_run" {
  type = bool
  default = null
}

variable "command" {
  type = list(string)
  default = null
}

variable "upload" {
  type = map(object({
    file = string
    content = optional(string)
    content_base64 = optional(string)
    executable = optional(bool)
    source = optional(string)
  }))
  default = null
}

variable "stack" {
  type = string
  default = null
}

variable "ports" {
  type = map(object({
    internal = number
    external = number
    protocol = string
  }))
  default = {}
}

variable "devices" {
  type = list(object({
    host_path = string
    container_path = optional(string)
    permissions = optional(string)
  }))
  default = null
}

variable "capabilities" {
  type = object({
    add = optional(list(string))
    drop = optional(list(string))
  })
  default = null
}

variable "stop_signal" {
  type = string
  default = null
}

variable "privileged" {
  type = bool
  default = null
}
